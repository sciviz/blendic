# blenDIC V1.2.0
2022-4-4

Improvements and fixes:
----------------------------
- Optional renumbering of cameras after deletion;
- Updated example file;
- Bug fixed in the export of the projection matrices;
- Imrpovement of the size of cameras at import;
- Major improvement of the depth of field module and computation of the measurable area for each pair of cameras;
- Updated user interface;
- "Debug mode" renamed "Advanced mode";
- Fixed import of chAruco targets;
- Cameras can be renumbered when one setup is deleted.

Novelties: 
--------------
- Add Markers: possibility to add markers on the **imported_mesh** (bowtie and circle) + *export_markers* operator;
- Add Pre-calibration points: add points on any mesh of the 3D scene + *export_precal* operator (directly readable by EikoTwin DIC);
- Pair/unpair cameras: create stereo systems by pairing cameras (targets are merged);
- Compute stereo angle between paired cameras:
- Context-dependent behavior of "x" and "DEL" to delete camera and dependencies or markers and pre-calibration points.

Description:
--------------
_**blenDIC**_ is a Blender add-on (additional module) developed by [Matthieu Vitse](https://www.sciviz.fr/) that provides optimized tools for the design and the optimization of digital image correlation experiments. It is based on the work of [[Vitse et al, 2020]](https://hal.archives-ouvertes.fr/hal-02456755) and [EikoSim](https://eikosim.com/)’s research team. 

_**blenDIC**_ is provided under a GPL (“free software”) license. 
See https://www.blender.org/about/license/ for more information on that matter. 
  
Please contact matthieu@sciviz.fr for support or training enquiries.

Requirements:
-------------
- Operating system: Microsoft’s Windows 10.
- Hardware: see https://www.blender.org/download/requirements/.
- Latest version of [Blender 3.1.0](https://www.blender.org/download/). Root permissions to the install folder are required to install dependecies. If you do not have such permissions on your machine, it is recommended to install a "portable" version of blender. 

Input and output: 
-----------------
**Input**:
- _Meshes_: .vtk (generated with EikoSim’s _EikoTwin Virtual_ converter). Note that these meshes are the external envelope of the 3D mesh, as a consequence there are only 2D elements (only tris, quads and tri-quad mixes are supported for now). 
- _Environment_: .vtk (generated with the _EikoTwin Virtual_ converter) or .stl.
- _Displacement files_ (one per step): .vtk (generated with the _EikoTwin Virtual_ converter).

**Output**: 
- Synthetic images (1 per camera per timestep).
- Projection matrices (one per camera).
- Markers location. 
- Pre-calibration points location.

Known issues: 
--------------
Section moved to the bug tracker, see https://gitlab.com/sciviz/blendic/-/issues. Please report any issues you may encounter on this tracker. 

Future improvments:
--------------------
- Improvement of the workflow between blenDIC and EikoTwin DIC
- Optimisation ot textures (speckle)
- Computation and display of sensitivity fields [[Vitse et al., 2020]](https://hal.archives-ouvertes.fr/hal-02456755)
- Support for binary vtk files

Useful links:
--------------
- Online help: https://www.sciviz.fr/blendic/ (ongoing work).
- Latest blender version: https://www.blender.org/download/.
- Academic paper on the design of virtual experiments: 
[Vitse et al, 2020] Toward virtual design and optimization of a structural test monitored by a multi-view system https://hal.archives-ouvertes.fr/hal-02456755.
- EikoSim blog posts about their use of blender (in French): 
         - https://eikosim.com/tout-sur-eikotwin-dic/conception-et-validation-dessais-dimpacts-laser/.
         - https://eikosim.com/articles-techniques/essai-structure-previsualisation/.
         - https://eikosim.com/articles-techniques/previsualisation-dessais-a-laide-du-logiciel-blender/.
- About Charuco: https://mecaruco2.readthedocs.io/en/latest/notebooks_rst/Aruco/sandbox/ludovic/aruco_calibration_rotation.html. 
- Blender stackexchange: https://blender.stackexchange.com/.



# Archive

_blenDIC V1.1.0
2021-6-30

Improvements and fixes:
----------------------------
- Increased rendering performances
- Default clip start at 0.001
- HD images (high res) by default
- Fixed undo crashing blender
- Set local camera views opens template_2
- Reorganized menus: Regular and Advanced
- Possibility to add mutliple environment files

Novelties: 
--------------
- Initial support for markers (bow tie or circle) operator (markers deform with the mesh)
- Initial support for pre-calibration points operator
- Added camera rigs (3D models) for the available presets
- Added example operator
- Added ground operator
- Initial support for depth of field module (aperture, )